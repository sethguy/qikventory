import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import './tag-slide.css';
import './sigh-result-form.css'
import { HashRouter } from 'react-router-dom'

import Main from './Components/Main.js'
import TagSlider from './Components/TagSlider.js'


import { Route } from 'react-router-dom'

import createHistory from 'history/createBrowserHistory'

const history = createHistory()

class App extends Component {

  render() {

    return (
      <HashRouter history={ history } basename='/'>
        <div className="App">
          <Route exact path="/" component={ Main } />
          <Route path="/tag-slider" component={ TagSlider } />
        </div>
      </HashRouter>
      );
  }

}

export default App;