import React, { Component } from 'react';

import { Route, Link } from 'react-router-dom'

import urlService from '../Services/urlService.js'

import { sightResponse } from './testData.js'


import restService from '../Services/restService.js'
import FloatingActionButton from 'material-ui/FloatingActionButton';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';


class Main extends Component {

  constructor(props) {

    super(props);

    this.state = {
      cams: [],
      loading: false,
      sightResults: [],
      sightImages: [],
      sightView: 'tags'
    };

    this.getMediaSources()
  //this.shape();
  }

  shape() {
    var data = {
      labelAnnotations: [],
      logoAnnotations: [],
      webDetection: {
        webEntities: [],
        visuallySimilarImages: []
      }
    }

    setTimeout(() => {

      this.googleResponseToSightResults({
        ...data,
        ...sightResponse
      })

    }, 1000)

  }

  logOut() {

    localStorage.clear();

    window.location.reload(true);

  }

  getMediaSources() {

    navigator.mediaDevices

      .enumerateDevices()

      .then((deviceInfos) => {

        this.setState({
          cams: deviceInfos
        })

      })

  } //getMediaSources

  selectVideoSource(source) {

    console.log('source', source)

    if (this.state.vidStream) this.state.vidStream.getTracks().forEach(track => track.stop())

    navigator.getUserMedia({
      video: {
        deviceId: {
          exact: source.deviceId
        }
      }
    }, (stream) => {

      this.setState({

        vidStream: stream,

        vidSrc: window.URL.createObjectURL(stream)
      })

    }, (err) => {
      alert("there was an error " + err)
    });

  }

  sendToVisionApi(content) {
    this.setState({
      loading: true
    }, () => {

      var qikSightBody = {
        "requests": [{
          "image": {
            "content": content
          },
          "features": [

            {
              "type": "IMAGE_PROPERTIES",
              "maxResults": 10
            },
            {
              "type": "LABEL_DETECTION",
              "maxResults": 10
            },
            {
              "type": "LOGO_DETECTION",
              "maxResults": 10
            },
            {
              "type": "WEB_DETECTION",
              "maxResults": 10
            }
          ]
        }]
      }

      var url = "https://vision.googleapis.com/v1/images:annotate" + "?" + "key=AIzaSyDC-cPlrFmoKKttyvBgS3g83tAiLLp_SUg"

      restService.post(url, qikSightBody)

        //.switchMap((qikResponse) => {

        //  return restService.post("/", qikResponse)

        // })

        .subscribe((qikResponse) => {

          console.log('qikGoogleResponse', qikResponse.responses[0])

          var data = {
            labelAnnotations: [],
            logoAnnotations: [],
            webDetection: {
              webEntities: [],
              visuallySimilarImages: []
            }
          }

          this.googleResponseToSightResults({
            ...data,
            ...qikResponse.responses[0]
          })

          this.setState({
            loading: false
          })

        })

    })

  }

  setSightView(sightView) {

    this.setState({
      sightView
    })

  }

  googleResponseToSightResults({labelAnnotations, logoAnnotations, imagePropertiesAnnotation, cropHintsAnnotation, webDetection}) {

    var logo = logoAnnotations.map(anno => ({
      name: anno.description
    })) || []

    var labels = labelAnnotations.map(anno => ({
      name: anno.description
    }))

    var webEntities = webDetection.webEntities.map(anno => ({
      name: anno.description
    }))

    var images = webDetection.visuallySimilarImages

    this.storeResults({
      sightResults: [...labels, ...webEntities],
      sightImages: [...images]
    })

    window.location.href = "/#/tag-slider"

  }

  storeResults(results) {

    localStorage.setItem('qik.resultData', JSON.stringify(results))

  }

  closeResults() {

    this.setState({
      sightResults: []
    })

  }

  snapPic() {

    var previewVideoElement = document.getElementById('previewVideoElement')

    var canvas = document.createElement("Canvas");

    var width = window.innerWidth; //previewVideoElement.getBoundingClientRect().width

    var height = window.innerHeight; //previewVideoElement.getBoundingClientRect().height

    canvas.height = height
    canvas.width = width

    canvas.getContext("2d").drawImage(previewVideoElement, 0, 0, width, height)

    this.sendToVisionApi(canvas.toDataURL().replace('data:image/png;base64,', ''))

  }

  render() {

    return (
      <MuiThemeProvider>
        <div className="wholeView flex-col">
          <div className="showView dropzone">
            <div className="qikLogoFrame">
              <p className="qik-logo">
                { !this.state.loading ?
                  <span>qik Ventory</span> : null }
                { this.state.loading ?
                  <span>Loading</span> : null }
              </p>
            </div>
            <div className="sight-frame">
              <FloatingActionButton onClick={ (event) => this.snapPic() } className="action-time">
                <img src="static/images/start.svg" />
              </FloatingActionButton>
              <div className="sight-frame-options">
                <FloatingActionButton className="action-time">
                  <img src="static/images/settings.svg" />
                </FloatingActionButton>
                { this
                    .state
                    .cams
                    .filter((device) => device.kind === "videoinput")
                    .map(
                      (data, i) => (
                        <div onClick={ (event) => this.selectVideoSource(data) } className="sight-options-source" key={ i }>
                          <FloatingActionButton className="sight-options-button">
                            <span style={ { "color": "white" } }>cam { i + 1 }</span>
                          </FloatingActionButton>
                        </div>
                      )
                  ) }
              </div>
              { !this.state.loading ?
                <video id="previewVideoElement" className='sight-frame-video' autoPlay src={ this.state.vidSrc } /> : null }
            </div>
          </div>
        </div>
      </MuiThemeProvider>

      );
  }

}

export default Main;