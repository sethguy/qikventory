import React, { Component } from 'react';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Chip from 'material-ui/Chip';
import TextField from 'material-ui/TextField';

import sightResultsFormService from '../Services/sightResultsFormService'
import Hammer from 'hammerjs'

class SightResultsForm extends Component {

  constructor(props) {

    super(props);

    this.state = {
      currentSelectedFeild: null
    };

    sightResultsFormService.subject

      .filter(sightResultsStrean => sightResultsStrean.tagSwipe && sightResultsStrean.tagSwipe.text)

      .subscribe((sightResultsStrean) => {

        this.onTagSelection(sightResultsStrean.tagSwipe)
      })

  }

  onTagSelection(tagSwipe) {

    if (this.state.currentSelectedFeild) {

      var input = document.getElementById(this.state.currentSelectedFeild)
      input.blur()

      var inputText = input.value = `${input.value} ${tagSwipe.text}`


    }
  }



  pickField(fieldKey, blur) {

    console.log(fieldKey)

    var input = document.getElementById(fieldKey)

    input.parentElement.parentElement.style.border = "4px solid rgb(0, 188, 212)"

    var feilds = document.getElementById('sight-result-form').childNodes

    for (var i = 0; i < feilds.length; ++i) {
      var feild = feilds[i];

      if (feild != input.parentElement.parentElement) {

        feild.style.border = "0"
      }
    }

    //input.parentElement.parentElement.style.backgroundColor = "rgb(0, 188, 212)"

    if (this.state.editMode) {

      blur = false
    }


    if (blur) input.blur()
    else {

      var sightresutlschips = document.getElementById('sight-resutls-chips')

      sightresutlschips.style.display = "none"

    }
    this.setState({
      currentSelectedFeild: fieldKey
    })
  }
  blurOut() {
    var sightresutlschips = document.getElementById('sight-resutls-chips')

    sightresutlschips.style.display = "flex"
    this.setState({
      editMode: false
    }, () => {

    })

  }

  componentDidMount() {

    Hammer(document.getElementById('sight-result-form'), {
      time: 5000
    })

      .on('pressup', (e) => {
        e.preventDefault()
        console.log(e.target.tagName)

        if (e.target.tagName == "INPUT") {

          this.setState({
            editMode: true
          }, () => {

            e.target.focus()


          })



        }

      })

  }

  render() {

    return (
      <div id="sight-result-form" className="sight-result-form">
        <div className="sight-result-form-feild">
          <TextField id="Name" onBlur={ (event) => this.blurOut() } onFocus={ (event) => this.pickField("Name", true) } className="TextField" floatingLabelFixed="true" floatingLabelText="Name" />
        </div>
        <div className="sight-result-form-feild">
          <TextField id="Tags" onBlur={ (event) => this.blurOut() } onFocus={ (event) => this.pickField("Tags", true) } className="TextField" floatingLabelFixed="true" floatingLabelText="Tags" />
        </div>
        <div className="sight-result-form-feild">
          <TextField id="Description" onBlur={ (event) => this.blurOut() } onFocus={ (event) => this.pickField("Description", true) } className="TextField" floatingLabelFixed="true" floatingLabelText="Description"
          />
        </div>
        <div className="sight-result-form-feild">
          <TextField id="Price" onBlur={ (event) => this.blurOut() } onFocus={ (event) => this.pickField("Price", false) } className="TextField" floatingLabelFixed="true" floatingLabelText="price" />
        </div>
      </div>

      );
  }

}

export default SightResultsForm;