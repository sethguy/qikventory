import React, { Component } from 'react';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Chip from 'material-ui/Chip';
import SightResultsForm from './SightResultsForm.js'
import interact from 'interactjs'
import sightResultsFormService from '../Services/sightResultsFormService'
import Hammer from 'hammerjs'

class TagSlider extends Component {
  constructor(props) {

    super(props);

    this.state = {
      product: {
        name: "",
        tags: [],
        price: 0
      },
      sightResults: [],
      sightImages: [],
      sightView: 'tags'
    };

    this.configInteractjs();

  }

  getResultData() {

    var resultDataString = localStorage.getItem('qik.resultData')

    if (resultDataString && resultDataString.length > 0) {

      var resultData = JSON.parse(resultDataString)
      this.setState(resultData)
    }

  }

  setSightView(sightView) {

    this.setState({
      sightView
    })

  }

  componentDidMount() {

    this.getResultData()


    Hammer(document.getElementById('sight-resutls-chips-scroll')).on('swipe', (e) => {
      this.sendTag(e)


    })

  }

  render() {

    return (
      <MuiThemeProvider>
        <div className="wholeView flex-col">
          <div className="showView">
            <div className="qikLogoFrame">
              <p className="qik-logo">
                { !this.state.loading ?
                  <span>qik Ventory</span> : null }
                { this.state.loading ?
                  <span>Loading</span> : null }
              </p>
            </div>
            <div className="sigthResults">
              <div className="sightFade"></div>
              <div className="sight-resutls-show">
                <div className="sight-resutls-options">
                  <FloatingActionButton style={ { "position": "relative", "margin": "1%" } } onClick={ (event) => window.history.back() }>
                    <span style={ { "position": "relative", "color": "white" } }>back</span>
                  </FloatingActionButton>
                  <br/>
                  <br/>
                  <FloatingActionButton className="action-time">
                    <img src="static/images/settings.svg" />
                  </FloatingActionButton>
                  <br/>
                  <br/>
                  { this.state.sightView === 'images' ?
                    
                    <FloatingActionButton style={ { "position": "relative", "margin": "1%" } } onClick={ (event) => this.setSightView('tags') }>
                      <span style={ { "position": "relative", "color": "white" } }>tags</span>
                    </FloatingActionButton>
                    :
                    <FloatingActionButton style={ { "position": "relative", "margin": "1%" } } onClick={ (event) => this.setSightView('images') }>
                      <span style={ { "position": "relative", "color": "white" } }>images</span>
                    </FloatingActionButton> }
                </div>
                <div id="tag-slider-container" className="tag-slider-container">
                  <SightResultsForm></SightResultsForm>
                  { this.state.sightView === 'images' ?
                    
                    <div className="sight-resutls-image-grid">
                      { this
                          .state
                          .sightImages
                          .map(
                            (data, i) => (
                        
                              <img src={ data.url } style={ { "position": "relative", height: "300px", width: "300px", margin: "1%" } } key={ i } />
                        
                            )
                        ) }
                    </div>
                    :
                    <div id="sight-resutls-chips" className="sight-resutls-chips">
                      <div id="sight-resutls-chips-scroll" className="sight-resutls-chips-scroll">
                        { this
                            .state
                            .sightResults
                            .map(
                              (data, i) => (
                                <div id={ i + "way" } className="big-chips dropable draggable">
                                  <p className="big-chips-text" style={ { "position": "relative", "color": "white", "fontSize": "10vw" } } key={ i }>
                                    { data.name }
                                  </p>
                                </div>
                              )
                          ) }
                      </div>
                    </div> }
                </div>
                <div className="sight-resutls-options">
                  <FloatingActionButton>
                    <img src="static/images/add.svg" />
                  </FloatingActionButton>
                </div>
              </div>
            </div>
          </div>
        </div>
      </MuiThemeProvider>

      );
  }

  sendTag(e) {

    console.log(e.target.className)

    var target = e.target;
    var targetClass = target.className;

    var tag = ""

    if (targetClass.indexOf('big-chips') > -1) {

      if (targetClass.indexOf('big-chips-text') > -1) {

        console.log(tag = e.target.innerHTML)

      } else {

        console.log(tag = e.target.childNodes[0].innerHTML)

      }

      if (tag.length > 0) {

        sightResultsFormService.subject.next({

          tagSwipe: {
            text: tag
          }
        })
      }

    }


  }


  configInteractjs() {

    var self = this;

    interact('.dropzone').dropzone({
      // only accept elements matching this CSS selector
      accept: '.dropable',
      // Require a 75% element overlap for a drop to be possible
      overlap: 0.75,

      // listen for drop related events:

      ondropactivate: function(event) {
        // add active dropzone feedback
        var draggableElement = event.relatedTarget,
          dropzoneElement = event.target;
      },
      ondragenter: function(event) {
        var draggableElement = event.relatedTarget,
          dropzoneElement = event.target;


      },
      ondragleave: function(event) {
        // remove the drop feedback style
        var draggableElement = event.relatedTarget,
          dropzoneElement = event.target;



      },
      ondrop: function(event) {
        var draggableElement = event.relatedTarget,
          dropzoneElement = event.target;

          //console.log('event', target.parentElement)

          // var other = document.getElementById(target.id.replace('countergo', 'way'))

          // event.target.parentElement.style.overflow = "visible"

          // other.parentNode.removeChild(other)
          // console.log('event', target, target.parentElement)

          // target.style.display = "none"

          //target = null;


      },
      ondropdeactivate: function(event) {
        // remove active dropzone feedback



      }
    });

    interact('.draggable')
      .draggable({
        // enable inertial throwing
        inertia: true,
        // keep the element within the area of it's parent
        restrict: {
          endOnly: true,
          elementRect: {
            top: 0,
            left: 0,
            bottom: 1,
            right: 1
          }
        },
        // enable autoScroll
        autoScroll: true,

        // call this function on every dragmove event
        onmove: dragMoveListener,
        // call this function on every dragend event
        onend: function(event) {
          console.log('end')

          //self.sendTag(event)

          // other.style.visibility = "hidden"

        }
      });

    function dragMoveListener(event) {

      var target = event.target,
        // keep the dragged position in the data-x/data-y attributes
        x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx;
        //  y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

      // translate the element
      target.style.webkitTransform = target.style.transform = 'translate(' + x + 'px, ' + 0 + 'px)';

      // update the posiion attributes
      // target.setAttribute('data-x', x);
      // target.setAttribute('data-y', y);

    }

    // this is used later in the resizing and gesture demos
    window.dragMoveListener = dragMoveListener;

  }

}

export default TagSlider;