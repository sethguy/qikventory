var sightResponse = {
  "labelAnnotations": [
    {
      "mid": "/m/0283dt1",
      "description": "mouth",
      "score": 0.7305116
    },
    {
      "mid": "/m/05r655",
      "description": "girl",
      "score": 0.72360384
    },
    {
      "mid": "/m/0dgw9r",
      "description": "human",
      "score": 0.70457774
    },
    {
      "mid": "/m/0ds99lh",
      "description": "fun",
      "score": 0.650902
    },
    {
      "mid": "/m/09cx8",
      "description": "finger",
      "score": 0.63094074
    },
    {
      "mid": "/m/0271t",
      "description": "drink",
      "score": 0.5888612
    },
    {
      "mid": "/m/01ykh",
      "description": "cuisine",
      "score": 0.568125
    },
    {
      "mid": "/m/02q08p0",
      "description": "dish",
      "score": 0.53944004
    }],
  "imagePropertiesAnnotation": {
    "dominantColors": {
      "colors": [
        {
          "color": {
            "red": 131,
            "green": 115,
            "blue": 97
          },
          "score": 0.16233332,
          "pixelFraction": 0.1315873
        },
        {
          "color": {
            "red": 20,
            "green": 12,
            "blue": 13
          },
          "score": 0.16011941,
          "pixelFraction": 0.21380952
        },
        {
          "color": {
            "red": 52,
            "green": 50,
            "blue": 70
          },
          "score": 0.10061683,
          "pixelFraction": 0.08433863
        },
        {
          "color": {
            "red": 77,
            "green": 45,
            "blue": 34
          },
          "score": 0.05891312,
          "pixelFraction": 0.07481481
        },
        {
          "color": {
            "red": 86,
            "green": 79,
            "blue": 86
          },
          "score": 0.058620024,
          "pixelFraction": 0.06037037
        },
        {
          "color": {
            "red": 39,
            "green": 17,
            "blue": 20
          },
          "score": 0.06779986,
          "pixelFraction": 0.054708995
        },
        {
          "color": {
            "red": 107,
            "green": 73,
            "blue": 57
          },
          "score": 0.052193683,
          "pixelFraction": 0.06015873
        },
        {
          "color": {
            "red": 58,
            "green": 47,
            "blue": 49
          },
          "score": 0.051223803,
          "pixelFraction": 0.05925926
        },
        {
          "color": {
            "red": 98,
            "green": 81,
            "blue": 64
          },
          "score": 0.04904763,
          "pixelFraction": 0.036931217
        },
        {
          "color": {
            "red": 25,
            "green": 24,
            "blue": 41
          },
          "score": 0.03655178,
          "pixelFraction": 0.029206349
        }]
    }
  },
  "cropHintsAnnotation": {
    "cropHints": [
      {
        "boundingPoly": {
          "vertices": [
            {},
            {
              "x": 1401
            },
            {
              "x": 1401,
              "y": 1180
            },
            {
              "y": 1180
            }]
        },
        "confidence": 1,
        "importanceFraction": 1.12
      }]
  },
  "webDetection": {
    "webEntities": [
      {
        "entityId": "/m/0283dt1",
        "score": 0.8694646,
        "description": "Mouth"
      },
      {
        "entityId": "/m/0271t",
        "score": 0.84993786,
        "description": "Drink"
      },
      {
        "entityId": "/m/01ykh",
        "score": 0.78871495,
        "description": "Cuisine"
      },
      {
        "entityId": "/m/0hjbz",
        "score": 0.7084,
        "description": "Drinking"
      },
      {
        "entityId": "/m/0dgw9r",
        "score": 0.697434,
        "description": "Human"
      }],
    "visuallySimilarImages": [
      {
        "url": "https://www.karenrusso.co.uk/f-users/user_100192/website_100174/images/thumbs/W_960_spontanious_human_combust2.jpg"
      },
      {
        "url": "https://i.ytimg.com/vi/wR5kMAHj2p4/hqdefault.jpg"
      },
      {
        "url": "http://cdn.cnn.com/cnnnext/dam/assets/121022103858-vo-china-boy-stuck-in-wall-00005730-story-top.jpg"
      },
      {
        "url": "https://i.pinimg.com/736x/39/7f/24/397f24f280d125d58c7a5e8f671cc328--human-being-kitty.jpg"
      },
      {
        "url": "https://i.ytimg.com/vi/UAJMXxomJTA/hqdefault.jpg"
      },
      {
        "url": "https://i.ytimg.com/vi/ZNSPSe9L5jM/maxresdefault.jpg"
      },
      {
        "url": "https://farm8.staticflickr.com/7399/11476077175_482d0ec1dc_b.jpg"
      },
      {
        "url": "https://i.ytimg.com/vi/oDmuzNFvLiQ/hqdefault.jpg"
      },
      {
        "url": "http://cdn.ebaumsworld.com/thumbs/2014/09/23/125416/84298560/smallkid.jpg"
      },
      {
        "url": "https://jessicamudditt.files.wordpress.com/2011/12/susan-1980_1.jpeg?w=350&h=200&crop=1"
      }]
  }
}

var mockGoogleSightResponse = function(sightResponse) {}

export { sightResponse }