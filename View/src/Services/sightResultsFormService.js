import { Subject } from 'rxjs'

var sightResultsFormSubject = new Subject()

var sightResultsFormService = {

  subject: sightResultsFormSubject
}

export default sightResultsFormService