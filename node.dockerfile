FROM node:latest

MAINTAINER Seth Terry

ENV NODE_ENV=development 

ENV NODE_HTTP_PORT=8080
ENV NODE_HTTPS_PORT=443

ENV SSL_START=ON

ENV MONGO_IP=mongodb

RUN npm install forever -g

COPY package.json /qikV/package.json

RUN cd qikV; npm install

ADD server.js /qikV/server.js

ADD View/build /qikV/View/build

ADD routes /qikV/routes

ADD middleWare /qikV/middleWare

ADD rxFormidable /qikV/rxFormidable

ADD Services /qikV/Services

ADD certs /qikV/certs

WORKDIR   /qikV

EXPOSE $NODE_HTTP_PORT

EXPOSE $NODE_HTTPS_PORT

ENTRYPOINT ["forever", "server.js"]